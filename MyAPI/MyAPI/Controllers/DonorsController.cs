﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyAPI;
using MyAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DonorsController : ControllerBase
    {

        [HttpGet()]
        public async Task<IActionResult> GetDonorsAsync([FromQuery] DonorFilterModel filter)
        {
            using (var db = new BasicMedCenterContext())
            {
                var query = db.Donors.AsQueryable();
                if (filter.Id.HasValue)
                {
                    query = db.Donors.Where(x => x.Id == filter.Id);
                }
                if (!string.IsNullOrWhiteSpace(filter.Name))
                {
                    query = db.Donors.Where(x => x.Name == filter.Name);
                }
                if (!string.IsNullOrWhiteSpace(filter.BloodGroup))
                {
                    query = db.Donors.Where(x => x.BloudGroup == filter.BloodGroup);
                }
                var donors = await query.Select(x => new DonorViewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    LastName = x.LastName,
                    BloudGroup = x.BloudGroup,
                    BloodDonation = x.BloodDonation,

                }).ToArrayAsync();
                return Ok(donors);
            }

        }


        [HttpPut("{id}")]

        public async Task<IActionResult> EditDonorAsync([FromRoute] int id, [FromBody] DonorEditModel edit)
        {
            using (var db = new BasicMedCenterContext())
            {
                var donorToEdit = await db.Donors.FirstOrDefaultAsync(x => x.Id == id);
                if (donorToEdit == null) return BadRequest();

                donorToEdit.PhoneNumber = String.IsNullOrWhiteSpace(edit.PhoneNumber) ? donorToEdit.PhoneNumber : edit.PhoneNumber;

                donorToEdit.BloodDonation = (edit.DateTime.Year == donorToEdit.DateTime.Year && donorToEdit.BloodDonation < 5) ? edit.BloodDonation : (donorToEdit.DateTime.Year != edit.DateTime.Year) ? edit.BloodDonation : donorToEdit.BloodDonation;

                donorToEdit.DateTime = (!string.IsNullOrWhiteSpace(edit.DateTime.ToString())) ? edit.DateTime : donorToEdit.DateTime;
                db.Donors.Update(donorToEdit);
                await db.SaveChangesAsync();
            }
            return Ok();
        }

        [HttpPost]

        public async Task<IActionResult> AddDonorsAsync([FromBody] DonorAddModel model)
        {
            if (!string.IsNullOrWhiteSpace(model.Name) || model.BloudGroup == null) return BadRequest();
            using (var db = new BasicMedCenterContext())
            {

                var donor = new Donor
                {
                    Name = model.Name,
                    LastName = model.LastName,
                    PhoneNumber = model.PhoneNumber,
                    BloudGroup = model.BloudGroup,
                    BloodDonation = model.BloodDonation,
                    DateTime = model.DateTime,

                };

                db.Donors.Add(donor);
                await db.SaveChangesAsync();
                return Created("", donor);
            };
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDonorAsync([FromRoute] int id)
        {
            using (var db = new BasicMedCenterContext())
            {
                var donor = await db.Donors.FirstOrDefaultAsync(x => x.Id == id);
                if (donor == null) return BadRequest();

                db.Donors.Remove(donor);
                await db.SaveChangesAsync();

            }
            return Ok();
        }
    }


}
