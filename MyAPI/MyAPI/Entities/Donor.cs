﻿using Microsoft.AspNetCore.Builder;

namespace MyAPI
{
    public class Donor
    {
        public int Id {  get; set; }    
        public string Name { get; set; }
        public string LastName { get; set; }
        public string BloudGroup { get; set; } //A,B,AB,O +-

        public string PhoneNumber { get; set; }

        public DateTime DateTime {  get; set; }

        public int BloodDonation {  get; set; } 


    }
}
