﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyAPI.Migrations
{
    public partial class FirstMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Donors",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "NVARCHAR(250)", nullable: false),
                    LastName = table.Column<string>(type: "NVARCHAR(250)", nullable: false),
                    BloudGroup = table.Column<string>(type: "VARCHAR(13)", nullable: false),
                    PhoneNumber = table.Column<string>(type: "NVARCHAR(25)", nullable: false),
                    DateTime = table.Column<DateTime>(type: "DATETIME", nullable: false),
                    BloodDonation = table.Column<int>(type: "INT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Donors", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Donors");
        }
    }
}
