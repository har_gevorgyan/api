﻿using Microsoft.EntityFrameworkCore;

namespace MyAPI
{
    public class BasicMedCenterContext:DbContext
    {
        public DbSet<Donor> Donors {  get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=HP;Database=BasicMedCenter;Trusted_Connection=true;");
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Donor>(x =>
            {
                x.ToTable("Donors");
                x.HasKey(x => x.Id);
                x.Property(x => x.Id).ValueGeneratedOnAdd();
                x.Property(x => x.Name).HasColumnType("NVARCHAR(250)");
                x.Property(x => x.LastName).HasColumnType("NVARCHAR(250)");
                x.Property(x => x.PhoneNumber).HasColumnType("NVARCHAR(25)");
                x.Property(x => x.BloudGroup).HasColumnType("VARCHAR(13)");
                x.Property(x => x.BloodDonation).HasColumnType("INT");
                x.Property(x => x.DateTime).HasColumnType("DATETIME");



            });
               
            base.OnModelCreating(modelBuilder);
        }
    }
}
