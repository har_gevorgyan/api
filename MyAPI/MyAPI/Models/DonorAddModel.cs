﻿using System.ComponentModel.DataAnnotations;

namespace MyAPI.Models
{
    public class DonorAddModel
    {
        [Required]
        [StringLength(25,ErrorMessage ="Not correct name")]
        public string Name { get; set; }

        [Required]
        [StringLength(25, ErrorMessage = "Not correct lastname")]
        public string LastName { get; set; }

        [Required]
        [StringLength(15,ErrorMessage ="Write A|B|AB|O+[Positive or Negativ]")]
       public string BloudGroup { get; set; } //A,B,AB,O +-

        public string PhoneNumber { get; set; }

        [Required]
        public DateTime DateTime { get; set; }

        [Range(0,5)]
        public int BloodDonation { get; set; }
    }
}
