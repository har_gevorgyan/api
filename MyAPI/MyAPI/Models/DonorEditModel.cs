﻿using System.ComponentModel.DataAnnotations;

namespace MyAPI.Models
{
    public class DonorEditModel
    {
     
        public string PhoneNumber { get; set; }

        public DateTime DateTime { get; set; }

        [Range(0,5)]
        public int BloodDonation { get; set; } 
    }
}
