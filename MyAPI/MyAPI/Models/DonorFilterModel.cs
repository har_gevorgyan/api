﻿namespace MyAPI.Models
{
    public class DonorFilterModel
    {
        public int? Id {  get; set; }

        public string Name {  get; set; }   

        public string BloodGroup {  get; set; } 
    }
}
