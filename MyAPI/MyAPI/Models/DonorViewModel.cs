﻿namespace MyAPI.Models
{
    public class DonorViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string BloudGroup { get; set; } //A,B,AB,O +-
        public int BloodDonation { get; set; }
    }
}
